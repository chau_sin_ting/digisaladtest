package com.digisaladtest.Adapter;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.digisaladtest.Interface.OnLoadMoreListener;
import com.digisaladtest.Interface.OnRecyclerItemClickListener;


/**
 * Created by kathychau on 13/4/2017.
 */
public class BaseRecyclerViewAdapter extends RecyclerView.Adapter {
    protected int visibleThreshold = 2;
    protected OnRecyclerItemClickListener onRecyclerItemClickListener;
    protected OnLoadMoreListener onLoadMoreListener;
    protected Context context;
    protected RecyclerView recyclerView;
    public boolean loading;

    public void setOnRecyclerItemClickListener(OnRecyclerItemClickListener onRecyclerItemClickListener) {
        this.onRecyclerItemClickListener = onRecyclerItemClickListener;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    protected Context getContext() {
        return context;
    }

    protected void setLoadMoreListener() {
        final RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            private int lastVisibleItem, totalItemCount;

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = layoutManager.getItemCount();
                if (layoutManager instanceof LinearLayoutManager)
                    lastVisibleItem = ((LinearLayoutManager) layoutManager).findLastVisibleItemPosition();
                else if (layoutManager instanceof GridLayoutManager)
                    lastVisibleItem = ((GridLayoutManager) layoutManager).findLastVisibleItemPosition();
                if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    loading = true;
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener.onLoadMore();
                    }

                }
            }
        });

    }
}
