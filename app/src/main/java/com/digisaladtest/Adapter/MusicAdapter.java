package com.digisaladtest.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.digisaladtest.Model.Music;
import com.digisaladtest.R;

import java.util.ArrayList;


/**
 * Created by kc8128 on 29/6/2017.
 */

public class MusicAdapter extends BaseRecyclerViewAdapter {
    private ArrayList<Music> musics;
    private static final int ITEM_VIEW_TYPE_ITEM = 1;

    public MusicAdapter(Context context, RecyclerView recyclerView, ArrayList<Music> musics) {
        this.musics = musics;
        this.context = context;
        this.recyclerView = recyclerView;
        setLoadMoreListener();
    }


    @Override
    public int getItemCount() {
        return musics.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_music, parent, false);
        vh = new MusicAdapter.ViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final MusicAdapter.ViewHolder viewHolder = (MusicAdapter.ViewHolder) holder;
        Music music = musics.get(position);
        viewHolder.tv_title.setText(music.getTrackName());
        viewHolder.tv_singer.setText(music.getArtistName() + " - " + music.getCollectionName());
        Glide.with(context).load(music.getBigThumbnail()).centerCrop().into(viewHolder.img_albumPreview);
    }

    @Override
    public int getItemViewType(int position) {
        return ITEM_VIEW_TYPE_ITEM;
    }


    private class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView img_albumPreview, btn_play;
        TextView tv_title, tv_singer;

        public ViewHolder(View itemView) {
            super(itemView);
            img_albumPreview = (ImageView) itemView.findViewById(R.id.img_albumPreview);
            btn_play = (ImageView) itemView.findViewById(R.id.btn_play);
            tv_title = (TextView) itemView.findViewById(R.id.tv_title);
            tv_singer = (TextView) itemView.findViewById(R.id.tv_singer);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            int id = view.getId();
            if (onRecyclerItemClickListener != null)
                onRecyclerItemClickListener.onItemClick(view, getLayoutPosition());
        }
    }

}
