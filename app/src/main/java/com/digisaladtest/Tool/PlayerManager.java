package com.digisaladtest.Tool;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;

import java.io.IOException;

/**
 * Created by kc8128 on 29/6/2017.
 */

public class PlayerManager extends AsyncTask<String, Void, Boolean> {
    private MediaPlayer mediaPlayer = new MediaPlayer();
    private boolean prepared = false;
    private Listener listener;

    public PlayerManager(Listener listener) {
        this.listener = listener;
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
    }

    @Override
    protected Boolean doInBackground(final String... params) {
        try {

            mediaPlayer.setDataSource(params[0]);
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer mp) {
                    listener.onCompletion();
                    mp.stop();
                    mp.reset();
                }
            });
            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer player) {
                    listener.onPrepared();
                    player.start();
                }
            });

            mediaPlayer.prepare();
            prepared = true;
        } catch (IllegalArgumentException e) {
            prepared = false;
            e.printStackTrace();
        } catch (SecurityException e) {
            prepared = false;
            e.printStackTrace();
        } catch (IllegalStateException e) {
            prepared = false;
            e.printStackTrace();
        } catch (IOException e) {
            prepared = false;
            e.printStackTrace();
        }
        return prepared;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        mediaPlayer.start();

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();


    }

    public void pause() {
        mediaPlayer.pause();
    }

    public void play() {
        mediaPlayer.start();
    }

    public boolean isPrepared() {
        return prepared;
    }

    public int getDuration() {
        return mediaPlayer.getDuration();
    }

    public int getCurrentPosition() {
        return mediaPlayer.getCurrentPosition();
    }

    public void seekTo(int progress) {
        mediaPlayer.seekTo(progress);
    }

    public void clear() {
        if (mediaPlayer != null) {
            mediaPlayer.reset();
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }

    public interface Listener {
        void onCompletion();

        void onPrepared();
    }

}
