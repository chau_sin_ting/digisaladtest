package com.digisaladtest.Interface;

import android.view.View;

/**
 * Created by kc8128 on 31/5/2017.
 */
public interface OnRecyclerItemClickListener {
    void onItemClick(View view, int position);
}
