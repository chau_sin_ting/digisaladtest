package com.digisaladtest.Interface;

/**
 * Created by kc8128 on 31/5/2017.
 */

public interface OnLoadMoreListener {
    void onLoadMore();
}
