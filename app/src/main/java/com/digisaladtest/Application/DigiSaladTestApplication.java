package com.digisaladtest.Application;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by kc8128 on 29/6/2017.
 */

public class DigiSaladTestApplication extends Application implements Application.ActivityLifecycleCallbacks {
    private final String TAG = "Application";
    public static boolean isDebugMode = true;
    public static final String PREFS_NAME = " Prefs";
    private static DigiSaladTestApplication singleton;
    public RequestQueue requestQueue;
    private int numRunningActivities = 0;
    private int createdActivities = 0;
    private boolean isInBackground = false;
//    private Tracker mTracker;

    public static DigiSaladTestApplication getInstance() {
        return singleton;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        registerActivityLifecycleCallbacks(this);
        singleton = this;

    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    public static SharedPreferences getSharedPreferences() {
        return singleton.getSharedPreferences(PREFS_NAME, 0);
    }

    public RequestQueue getRequestQueue() {
        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return requestQueue;
    }

//    /**
//     * Gets the default {@link Tracker} for this {@link Application}.
//     *
//     * @return tracker
//     */
//    synchronized public Tracker getDefaultTracker() {
//        if (mTracker == null) {
//            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
//            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
//            mTracker = analytics.newTracker(R.xml.global_tracker);
//        }
//        return mTracker;
//    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
        createdActivities++;
    }

    @Override
    public void onActivityStarted(final Activity activity) {
        Log.i(TAG, "Activity Started: " + activity.getClass().getCanonicalName());
        numRunningActivities++;
        if (isInBackground) {
        }
    }

    @Override
    public void onActivityResumed(Activity activity) {

    }

    @Override
    public void onActivityPaused(Activity activity) {

    }

    @Override
    public void onActivityStopped(Activity activity) {
        Log.i(TAG, "Activity Stopped: " + activity.getClass().getCanonicalName());
        numRunningActivities--;

        if (numRunningActivities == 0) {
            Log.i(TAG, "No running activities left, app has likely entered the background.");
            isInBackground = true;
        } else {
            Log.i(TAG, numRunningActivities + " activities remaining");
        }
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        createdActivities--;
        if (createdActivities == 0) {
            isInBackground = false;
        }
    }
}
