package com.digisaladtest.Network;

/**
 * Created by kathychau on 23/11/15.
 */
public class NetworkParam {

    public String key;
    public String value;

    public NetworkParam(String key, String value)
    {
        this.key = key;
        this.value = value;
    }
}
