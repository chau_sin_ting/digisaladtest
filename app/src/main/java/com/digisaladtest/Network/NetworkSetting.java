package com.digisaladtest.Network;


import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.digisaladtest.Application.DigiSaladTestApplication;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by kathychau on 23/11/15.
 */
public class NetworkSetting {
    public final static int MY_SOCKET_TIMEOUT_MS = 5000;

    public static void postRequest(final int timeout, final String jsonData, final String url, final NetworkResponseListener listener) {
        try {
            RequestQueue queue = DigiSaladTestApplication.getInstance().getRequestQueue();
            StringRequest req = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    VolleyLog.v("Response:%n %s", response);
                    if (listener != null) {
                        listener.response(response);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error.networkResponse != null) {
                        if (error.networkResponse.statusCode == 401 || error.networkResponse.statusCode == 500) {
                            String displayMessage = null;
                            if (error.networkResponse.data != null) {
                                displayMessage = new String(error.networkResponse.data);
                                displayMessage = trimMessage(displayMessage, "displayMessage");
                            }
                            listener.error(new NetworkResponseListener.Error(error.networkResponse.statusCode, displayMessage));
                        } else {
                            listener.error(new NetworkResponseListener.Error(-1, null));
                        }
                    } else {
                        listener.error(new NetworkResponseListener.Error(503, null));
                    }
                }
            }) {

                @Override
                public byte[] getBody() throws AuthFailureError {
                    if (jsonData != null)
                        return jsonData.getBytes();
                    return super.getBody();
                }

            };

            req.setRetryPolicy(new DefaultRetryPolicy(timeout,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(req);
        } catch (Exception exception) {
            System.exit(0);
        }
    }

    public static void getRequest(final int timeout, final String url, final NetworkResponseListener listener) {
        try {
            RequestQueue queue = DigiSaladTestApplication.getInstance().getRequestQueue();
            StringRequest req = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    VolleyLog.v("Response:%n %s", response);
                    if (listener != null) {
                        listener.response(response);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error.networkResponse != null) {
                        if (error.networkResponse.statusCode == 401 || error.networkResponse.statusCode == 500) {
                            String displayMessage = null;
                            if (error.networkResponse.data != null) {
                                displayMessage = new String(error.networkResponse.data);
                                displayMessage = trimMessage(displayMessage, "displayMessage");
                            }
                            listener.error(new NetworkResponseListener.Error(error.networkResponse.statusCode, displayMessage));
                        } else {
                            listener.error(new NetworkResponseListener.Error(-1, null));
                        }
                    } else {
                        listener.error(new NetworkResponseListener.Error(503, null));
                    }
                }
            });

            req.setRetryPolicy(new DefaultRetryPolicy(timeout,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(req);
        } catch (Exception exception) {
            System.exit(0);
        }

    }

    public static String trimMessage(String json, String key) {
        String trimmedString = null;

        try {
            JSONObject obj = new JSONObject(json);
            trimmedString = obj.getString(key);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

        return trimmedString;
    }

}

