package com.digisaladtest.Network;

/**
 * Created by kathychau on 23/11/15.
 */
public interface NetworkResponseListener {
    void response(String response);

    void error(Error error);

    class Error {
        private int code;
        private String message;

        public Error(int code, String message) {
            this.code = code;
            this.message = message;
        }

        public int getCode() {
            return code;
        }

        public String getMessage() {
            return message;
        }
    }
}

