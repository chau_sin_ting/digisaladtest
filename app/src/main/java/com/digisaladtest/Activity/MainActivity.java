package com.digisaladtest.Activity;

import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.digisaladtest.API.APIManger;
import com.digisaladtest.API.JsonTag;
import com.digisaladtest.Adapter.MusicAdapter;
import com.digisaladtest.Interface.OnRecyclerItemClickListener;
import com.digisaladtest.Model.Music;
import com.digisaladtest.Network.NetworkResponseListener;
import com.digisaladtest.R;
import com.digisaladtest.Tool.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends RecyclerViewActivity {
    private final String TAG = "Main";
    private EditText et_searchBar;
    private ArrayList<Music> musics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        connectView();
        initView();
        setListeners();
    }

    @Override
    protected void connectView() {
        super.connectView();
        et_searchBar = (EditText) findViewById(R.id.et_searchBar);
    }

    @Override
    protected void initView() {
        super.initView();
    }


    @Override
    protected void setListeners() {
        super.setListeners();
        ((MusicAdapter) adapter).setOnRecyclerItemClickListener(new OnRecyclerItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(MainActivity.this, PreviewTrackActivity.class);
                intent.putExtra(Constants.ARG_MUSIC, musics.get(position));
                startActivity(intent);
            }
        });
        et_searchBar.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    Log.i(TAG, "Enter pressed");
                    searchMusic();
                }
                return false;
            }
        });
    }

    @Override
    protected void configLayoutManager() {
        super.configLayoutManager();
        int smallPaddingInPixels = getResources().getDimensionPixelSize(R.dimen.bit_padding);
        mRecyclerView.addItemDecoration(new MainActivity.SpacesItemDecoration(smallPaddingInPixels));
        mLayoutManager = new LinearLayoutManager(this);
    }

    @Override
    protected void configAdapter() {
        super.configAdapter();
        musics = new ArrayList<>();
        adapter = new MusicAdapter(this, mRecyclerView, musics);
    }


    private void searchMusic() {
        musics.clear();
        APIManger.search(et_searchBar.getText().toString(), searchMusicListener);
    }

    private NetworkResponseListener searchMusicListener = new NetworkResponseListener() {
        @Override
        public void response(String response) {
            try {
                JSONObject jsonResponse = new JSONObject(response);
                musics.addAll(Music.getMusics(jsonResponse.getJSONArray(JsonTag.results)));
                adapter.notifyDataSetChanged();
            } catch (JSONException e) {

            }
        }

        @Override
        public void error(Error error) {

        }

    };


    //set spacing of recycle view
    public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int space;

        public SpacesItemDecoration(int space) {
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {


            outRect.bottom = space;
            outRect.left = space;
            outRect.right = space;

        }
    }
}
