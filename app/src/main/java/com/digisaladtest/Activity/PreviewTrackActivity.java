package com.digisaladtest.Activity;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.digisaladtest.Model.Music;
import com.digisaladtest.R;
import com.digisaladtest.Tool.PlayerManager;
import com.liuguangqiang.swipeback.SwipeBackActivity;
import com.liuguangqiang.swipeback.SwipeBackLayout;

import static com.digisaladtest.Tool.Constants.ARG_MUSIC;

/**
 * Created by kc8128 on 29/6/2017.
 */

public class PreviewTrackActivity extends SwipeBackActivity implements View.OnClickListener{
    private TextView tv_title, tv_singer;
    private ImageView img_thumbnail, btn_play;
    private SeekBar seekBar;
    private Music music;
    private PlayerManager playerManager = null;
    private Handler seekHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview_track);
        connectView();
        initView();
        setListeners();
    }

    @Override
    protected void onDestroy() {
        playerManager.clear();
        seekHandler.removeCallbacks(run);
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (playerManager.isPrepared()) {
            btn_play.setSelected(false);
            playerManager.pause();
        }
    }


    protected void connectView() {
        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_singer = (TextView) findViewById(R.id.tv_singer);
        img_thumbnail = (ImageView) findViewById(R.id.img_thumbnail);
        btn_play = (ImageView) findViewById(R.id.btn_play);
        seekBar = (SeekBar) findViewById(R.id.seekBar);
    }

    protected void initView() {
        setDragEdge(SwipeBackLayout.DragEdge.TOP);
        music = (Music) getIntent().getSerializableExtra(ARG_MUSIC);
        tv_title.setText(music.getTrackName());
        tv_title.setSelected(true);
        tv_singer.setText(music.getArtistName() + " - " + music.getCollectionName());
        tv_singer.setSelected(true);
        Glide.with(this).load(music.getBigThumbnail()).centerCrop().into(img_thumbnail);
        setUpPlayer();
    }


    protected void setListeners() {
        btn_play.setOnClickListener(this);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                playerManager.seekTo(seekBar.getProgress());
            }
        });
    }

    private void setUpPlayer() {
        playerManager = new PlayerManager(new PlayerManager.Listener() {
            @Override
            public void onCompletion() {
                btn_play.setSelected(false);
                setUpPlayer();
            }

            @Override
            public void onPrepared() {
                seekBar.setMax(playerManager.getDuration());
                UpdateSeekBar();
            }
        });

    }

    private Runnable run = new Runnable() {
        @Override
        public void run() {
            UpdateSeekBar();
        }
    };

    public void UpdateSeekBar() {
        seekBar.setProgress(playerManager.getCurrentPosition());
        seekHandler.postDelayed(run, 500);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_play:
                if (!btn_play.isSelected()) {
                    btn_play.setSelected(true);
                    if (!playerManager.isPrepared())
                        playerManager.execute(music.getPreviewUrl());
                    else
                        playerManager.play();
                } else {
                    btn_play.setSelected(false);
                    playerManager.pause();
                }
                break;
            default:
                break;
        }

    }
}

