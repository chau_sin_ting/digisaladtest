package com.digisaladtest.Activity;

import android.app.Activity;

/**
 * Created by kc8128 on 29/6/2017.
 */

public class BaseActivity extends Activity{
    protected void connectView() {
    }

    protected void initView() {
    }

    protected void setListeners() {
    }
}
