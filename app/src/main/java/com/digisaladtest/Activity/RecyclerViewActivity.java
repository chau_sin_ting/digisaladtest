package com.digisaladtest.Activity;

import android.support.v7.widget.RecyclerView;

import com.digisaladtest.R;

/**
 * Created by kc8128 on 29/6/2017.
 */

public class RecyclerViewActivity extends BaseActivity {
    protected RecyclerView mRecyclerView;
    protected RecyclerView.Adapter adapter;
    protected RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void connectView() {
        super.connectView();
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerview);
    }

    protected void configLayoutManager() {

    }

    protected void configAdapter() {

    }

    protected void refreshItems() {

    }



    @Override
    protected void initView() {
        super.initView();
        mRecyclerView.setHasFixedSize(true);
        configLayoutManager();
        mRecyclerView.setLayoutManager(mLayoutManager);
        configAdapter();
        mRecyclerView.setAdapter(adapter);
    }
}
