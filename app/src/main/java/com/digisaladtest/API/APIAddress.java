package com.digisaladtest.API;


/**
 * Created by kathychau on 24/11/15.
 */
public class APIAddress {

    public final static String Domain = "https://itunes.apple.com";

    public final static String BaseUrl = Domain;

    public final static String Search = BaseUrl + "/search";
}
