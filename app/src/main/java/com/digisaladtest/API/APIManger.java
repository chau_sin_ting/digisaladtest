package com.digisaladtest.API;

import com.digisaladtest.Network.NetworkResponseListener;
import com.digisaladtest.Network.NetworkSetting;

/**
 * Created by kc8218 on 14/6/2016.
 */
public class APIManger {
    public static void search(String keywords, NetworkResponseListener listener) {
        NetworkSetting.getRequest(NetworkSetting.MY_SOCKET_TIMEOUT_MS * 4, APIAddress.Search + "?entity=song&term=" + keywords.replaceAll(" ", "+"), listener);
    }
}
