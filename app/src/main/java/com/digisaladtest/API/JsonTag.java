package com.digisaladtest.API;

/**
 * Created by kc8218 on 14/6/2016.
 */
public class JsonTag {
    public static String results = "results";
    public static String artistName = "artistName";
    public static String collectionName = "collectionName";
    public static String trackName = "trackName";
    public static String previewUrl = "previewUrl";
    public static String artworkUrl60 = "artworkUrl60";
    public static String artworkUrl100 = "artworkUrl100";

}
