package com.digisaladtest.Model;

import com.digisaladtest.API.JsonTag;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by kc8128 on 29/6/2017.
 */

public class Music implements Serializable {
    private String artistName;
    private String collectionName;
    private String trackName;
    private String previewUrl;
    private String thumbnail;
    private String bigThumbnail;

    public Music(JSONObject data) {
        this.artistName = data.optString(JsonTag.artistName);
        this.collectionName = data.optString(JsonTag.collectionName);
        this.trackName = data.optString(JsonTag.trackName);
        this.previewUrl = data.optString(JsonTag.previewUrl);
        this.thumbnail = data.optString(JsonTag.artworkUrl60);
        this.bigThumbnail = data.optString(JsonTag.artworkUrl100);
    }

    public static ArrayList<Music> getMusics(JSONArray data) {
        ArrayList<Music> musics = new ArrayList<>();
        for (int i = 0; i < data.length(); i++)
            musics.add(new Music(data.optJSONObject(i)));
        return musics;
    }

    public String getArtistName() {
        return artistName;
    }

    public String getCollectionName() {
        return collectionName;
    }

    public String getTrackName() {
        return trackName;
    }

    public String getPreviewUrl() {
        return previewUrl;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public String getBigThumbnail() {
        return bigThumbnail;
    }
}
